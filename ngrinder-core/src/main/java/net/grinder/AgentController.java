/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package net.grinder;

<<<<<<< HEAD
import static org.ngrinder.common.util.NoOp.noOp;
import static org.ngrinder.common.util.Preconditions.checkNotNull;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import net.grinder.AgentDaemon.AgentShutDownListener;
import net.grinder.common.GrinderException;
import net.grinder.common.GrinderProperties;
import net.grinder.communication.AddressAwareMessage;
import net.grinder.communication.AgentControllerCommunicationDefauts;
import net.grinder.communication.ClientReceiver;
import net.grinder.communication.ClientSender;
import net.grinder.communication.CommunicationException;
import net.grinder.communication.ConnectionType;
import net.grinder.communication.Connector;
import net.grinder.communication.FanOutStreamSender;
import net.grinder.communication.MessageDispatchSender;
import net.grinder.communication.MessagePump;
import net.grinder.engine.agent.Agent;
import net.grinder.engine.common.AgentControllerConnectorFactory;
import net.grinder.engine.communication.AgentControllerServerListener;
=======
import net.grinder.AgentDaemon.AgentShutDownListener;
import net.grinder.common.GrinderException;
import net.grinder.common.GrinderProperties;
import net.grinder.communication.*;
import net.grinder.engine.agent.Agent;
import net.grinder.engine.common.AgentControllerConnectorFactory;
import net.grinder.engine.communication.AgentControllerServerListener;
import net.grinder.engine.communication.AgentDownloadGrinderMessage;
import net.grinder.engine.communication.AgentUpdateGrinderMessage;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import net.grinder.engine.communication.LogReportGrinderMessage;
import net.grinder.engine.controller.AgentControllerIdentityImplementation;
import net.grinder.message.console.AgentControllerProcessReportMessage;
import net.grinder.message.console.AgentControllerState;
import net.grinder.messages.agent.StartGrinderMessage;
import net.grinder.messages.console.AgentAddress;
<<<<<<< HEAD
import net.grinder.util.LogCompressUtil;
import net.grinder.util.NetworkUtil;
import net.grinder.util.thread.Condition;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.ngrinder.infra.AgentConfig;
import org.ngrinder.monitor.agent.collector.AgentSystemDataCollector;
=======
import net.grinder.util.LogCompressUtils;
import net.grinder.util.NetworkUtils;
import net.grinder.util.thread.Condition;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.ngrinder.common.constants.AgentConstants;
import org.ngrinder.infra.AgentConfig;
import org.ngrinder.monitor.collector.SystemDataCollector;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.monitor.controller.model.SystemDataModel;
import org.ngrinder.monitor.share.domain.SystemInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

<<<<<<< HEAD
/**
 * Agent Controller which handles agent start and stop.
 * 
 * @author JunHo Yoon
 * @since 3.0
 */
public class AgentController implements Agent {

	private static final Logger LOGGER = LoggerFactory.getLogger("agent controller");

	private Timer m_timer;
=======
import java.io.File;
import java.io.FilenameFilter;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import static org.ngrinder.common.constants.InternalConstants.PROP_INTERNAL_NGRINDER_VERSION;
import static org.ngrinder.common.util.NoOp.noOp;
import static org.ngrinder.common.util.Preconditions.checkNotNull;

/**
 * Agent Controller which handles agent start and stop.
 *
 * @author JunHo Yoon
 * @since 3.0
 */
public class AgentController implements Agent, AgentConstants {

	private static final Logger LOGGER = LoggerFactory.getLogger("agent controller");
	private final AgentConfig agentConfig;

	private Timer m_timer;
	@SuppressWarnings("FieldCanBeLocal")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private final Condition m_eventSynchronisation = new Condition();
	private final AgentControllerIdentityImplementation m_agentIdentity;
	private final AgentControllerServerListener m_agentControllerServerListener;
	private FanOutStreamSender m_fanOutStreamSender;
	private final AgentControllerConnectorFactory m_connectorFactory = new AgentControllerConnectorFactory(
<<<<<<< HEAD
					ConnectionType.AGENT);
	private AgentConfig agentConfig;
	private final Condition m_eventSyncCondition;
	private volatile AgentControllerState m_state = AgentControllerState.STARTED;

	private GrinderProperties m_grinderProperties;

	private AgentSystemDataCollector agentSystemDataCollector = new AgentSystemDataCollector();
=======
			ConnectionType.AGENT);
	private final Condition m_eventSyncCondition;
	private volatile AgentControllerState m_state = AgentControllerState.STARTED;

	private SystemDataCollector agentSystemDataCollector = new SystemDataCollector();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	private int m_connectionPort = 0;

	private static SystemDataModel emptySystemDataModel = new SystemDataModel();

	private AgentUpdateHandler agentUpdateHandler;

<<<<<<< HEAD
=======
	private int retryCount = 0;

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private String version;

	/**
	 * Constructor.
<<<<<<< HEAD
	 * 
	 * @param eventSyncCondition
	 *            event sync condition to wait until agent start to run.
	 * @param currentIp
	 *            current ip
	 * @throws GrinderException
	 *             If an error occurs.
	 */
	public AgentController(Condition eventSyncCondition, String currentIp) throws GrinderException {
		m_eventSyncCondition = eventSyncCondition;
		m_agentControllerServerListener = new AgentControllerServerListener(m_eventSynchronisation, LOGGER);
		// Set it with the default name 
		m_agentIdentity = new AgentControllerIdentityImplementation(NetworkUtil.getLocalHostName(), currentIp);
		agentSystemDataCollector = new AgentSystemDataCollector();
		agentSystemDataCollector.refresh();
	}

	/**
	 * Run agent controller. This method use default server port (for test)
	 * 
	 * @exception GrinderException
	 *                occurs when there is a problem.
	 */
	public void run() throws GrinderException {
		GrinderProperties grinderProperties = new GrinderProperties();
		grinderProperties.setInt(AgentConfig.AGENT_CONTROLER_SERVER_PORT,
						AgentControllerCommunicationDefauts.DEFAULT_AGENT_CONTROLLER_SERVER_PORT);
		synchronized (m_eventSyncCondition) {
			m_eventSyncCondition.notifyAll();
		}
		run(grinderProperties, 1);
	}

	/**
	 * Run the agent controller.
	 * 
	 * @param grinderProperties
	 *            {@link GrinderProperties} used.
	 * @param logCount
	 *            log count
	 * @throws GrinderException
	 *             occurs when the test execution is failed.
	 */
	public void run(GrinderProperties grinderProperties, long logCount) throws GrinderException {
=======
	 *
	 * @param eventSyncCondition event sync condition to wait until agent start to run.
	 */
	public AgentController(Condition eventSyncCondition, AgentConfig agentConfig) throws GrinderException {
		this.m_eventSyncCondition = eventSyncCondition;

		this.agentConfig = agentConfig;
		this.version = agentConfig.getInternalProperties().getProperty(PROP_INTERNAL_NGRINDER_VERSION);
		this.m_agentControllerServerListener = new AgentControllerServerListener(m_eventSynchronisation, LOGGER);
		// Set it with the default name
		this.m_agentIdentity = new AgentControllerIdentityImplementation(agentConfig.getAgentHostID(), NetworkUtils.DEFAULT_LOCAL_HOST_ADDRESS);
		this.m_agentIdentity.setRegion(agentConfig.getRegion());
		this.agentSystemDataCollector = new SystemDataCollector();
		this.agentSystemDataCollector.setAgentHome(agentConfig.getHome().getDirectory());
		this.agentSystemDataCollector.refresh();
	}


	/**
	 * Run the agent controller.
	 *
	 * @throws GrinderException occurs when the test execution is failed.
	 */
	@SuppressWarnings("ConstantConditions")
	public void run() throws GrinderException {
		synchronized (m_eventSyncCondition) {
			m_eventSyncCondition.notifyAll();
		}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		StartGrinderMessage startMessage = null;
		ConsoleCommunication consoleCommunication = null;
		m_fanOutStreamSender = new FanOutStreamSender(GrinderConstants.AGENT_CONTROLLER_FANOUT_STREAM_THREAD_COUNT);
		m_timer = new Timer(false);
<<<<<<< HEAD
		AgentDaemon agent = new AgentDaemon(checkNotNull(getAgentConfig(),
						"agentconfig should be provided before agent daemon start."));

		m_grinderProperties = grinderProperties;
		try {
			while (true) {
				do {
					m_agentIdentity.setName(agentConfig.getProperty(AgentConfig.AGENT_HOSTID,
									NetworkUtil.getLocalHostName()));
					m_agentIdentity.setRegion(agentConfig.getProperty(AgentConfig.AGENT_REGION, ""));
					final Connector connector = m_connectorFactory.create(m_grinderProperties);

					if (consoleCommunication == null) {
						try {
							consoleCommunication = new ConsoleCommunication(connector);
							consoleCommunication.start();
							LOGGER.info("connected to agent controller server at {}", connector.getEndpointAsString());
						} catch (CommunicationException e) {
							LOGGER.error("Error while connecting to controller {}", connector.getEndpointAsString());
							LOGGER.debug(e.getMessage(), e);
=======
		AgentDaemon agent = new AgentDaemon(checkNotNull(agentConfig,
				"agent.conf should be provided before agent daemon start."));
		try {
			while (true) {
				do {
					if (consoleCommunication == null) {
						final Connector connector = m_connectorFactory.create(agentConfig.getControllerIP(), agentConfig.getControllerPort());
						try {
							consoleCommunication = new ConsoleCommunication(connector);
							consoleCommunication.start();
							LOGGER.info("Connected to agent controller server at {}", connector.getEndpointAsString());
						} catch (CommunicationException e) {
							LOGGER.error("Error while connecting to agent controller server at {}",
									connector.getEndpointAsString());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
							return;
						}
					}

					if (consoleCommunication != null && startMessage == null) {
<<<<<<< HEAD
						LOGGER.info("waiting for agent controller server signal");
						m_state = AgentControllerState.READY;
						m_agentControllerServerListener.waitForMessage();

						if (m_agentControllerServerListener.received(AgentControllerServerListener.START)) {
							startMessage = m_agentControllerServerListener.getLastStartGrinderMessage();

							LOGGER.info("agent start message is revcieved from console {}", startMessage);
							continue;
						} else {
							break; // Another message, check at end of outer
									// while loop.
=======
						if (m_state == AgentControllerState.UPDATING) {
							m_agentControllerServerListener.waitForMessage();
							break;
						} else {
							LOGGER.info("Waiting for agent controller server signal");
							m_state = AgentControllerState.READY;
							m_agentControllerServerListener.waitForMessage();
							if (m_agentControllerServerListener.received(AgentControllerServerListener.START)) {
								startMessage = m_agentControllerServerListener.getLastStartGrinderMessage();
								LOGGER.info("Agent start message is received from controller {}", startMessage);
								continue;
							} else {
								break; // Another message, check at end of outer
								// while loop.
							}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
						}
					}

					if (startMessage != null) {
						m_agentIdentity.setNumber(startMessage.getAgentNumber());
					}
				} while (checkNotNull(startMessage).getProperties() == null);

				// Here the agent run code goes..
				if (startMessage != null) {
<<<<<<< HEAD
					final String testId = startMessage.getProperties().getProperty("grinder.test.id", "");
					LOGGER.info("starting agent... for {}", testId);
=======
					final String testId = startMessage.getProperties().getProperty("grinder.test.id", "unknown");
					LOGGER.info("Starting agent... for {}", testId);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					m_state = AgentControllerState.BUSY;
					m_connectionPort = startMessage.getProperties().getInt(GrinderProperties.CONSOLE_PORT, 0);
					agent.run(startMessage.getProperties());

					final ConsoleCommunication conCom = consoleCommunication;
					agent.resetListeners();
					agent.addListener(new AgentShutDownListener() {
						@Override
						public void shutdownAgent() {
<<<<<<< HEAD
							LOGGER.info("send log for {}", testId);
=======
							LOGGER.info("Send log for {}", testId);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
							sendLog(conCom, testId);
							m_state = AgentControllerState.READY;
							m_connectionPort = 0;
						}
					});
				}
				// Ignore any pending start messages.
				m_agentControllerServerListener.discardMessages(AgentControllerServerListener.START);

				if (!m_agentControllerServerListener.received(AgentControllerServerListener.ANY)) {
					// We've got here naturally, without a console signal.
<<<<<<< HEAD
					LOGGER.info("agent is started. waiting for agent controller signal");
=======
					LOGGER.info("Agent is started. Waiting for agent controller signal");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					m_agentControllerServerListener.waitForMessage();
				}

				if (m_agentControllerServerListener.received(AgentControllerServerListener.START)) {
					startMessage = m_agentControllerServerListener.getLastStartGrinderMessage();
				} else if (m_agentControllerServerListener.received(AgentControllerServerListener.STOP)) {
					agent.shutdown();
					startMessage = null;
					m_connectionPort = 0;
					m_agentControllerServerListener.discardMessages(AgentControllerServerListener.STOP);
				} else if (m_agentControllerServerListener.received(AgentControllerServerListener.SHUTDOWN)) {
<<<<<<< HEAD
					startMessage = null;
					m_connectionPort = 0;
					break;
				} else if (m_agentControllerServerListener.received(AgentControllerServerListener.UPDATE_AGENT)) {
=======
					m_connectionPort = 0;
					break;
				} else if (m_agentControllerServerListener.received(AgentControllerServerListener.AGENT_UPDATE)) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					// Do update agent by downloading new version.
					startMessage = null;
					m_connectionPort = 0;
					m_state = AgentControllerState.UPDATING;
<<<<<<< HEAD
					sendCurrentState(consoleCommunication);
					agentUpdateHandler = new AgentUpdateHandler(agentConfig);
					agentUpdateHandler.updateAgent(m_agentControllerServerListener.getLastUpdateAgentGrinderMessage());
				} else if (m_agentControllerServerListener.received(AgentControllerServerListener.LOG_REPORT)) {
					startMessage = null;
					m_state = AgentControllerState.BUSY;
					sendCurrentState(consoleCommunication);
					// Do update
=======
					final AgentUpdateGrinderMessage message = m_agentControllerServerListener.getLastAgentUpdateGrinderMessage();
					m_agentControllerServerListener.discardMessages(AgentControllerServerListener.AGENT_UPDATE);
					AgentDownloadGrinderMessage agentDownloadGrinderMessage = new AgentDownloadGrinderMessage(message.getVersion());
					try {
						// If it's initial message
						if (agentUpdateHandler == null && message.getNext() == 0) {
							IOUtils.closeQuietly(agentUpdateHandler);
							agentUpdateHandler = new AgentUpdateHandler(agentConfig, message);
						} else if (agentUpdateHandler != null) {
							if (message.isValid()) {
								retryCount = 0;
								agentUpdateHandler.update(message);
								agentDownloadGrinderMessage.setNext(message.getNext());
							} else if (retryCount <= AgentDownloadGrinderMessage.MAX_RETRY_COUNT) {
								retryCount++;
								agentDownloadGrinderMessage.setNext(message.getOffset());
							} else {
								throw new CommunicationException("Error while getting the agent package from " +
										"controller");
							}
						} else {
							throw new CommunicationException("Error while getting the agent package from controller");
						}
						if (consoleCommunication != null) {
							consoleCommunication.sendMessage(agentDownloadGrinderMessage);
						} else {
							break;
						}

					} catch (IllegalArgumentException ex) {
						IOUtils.closeQuietly(agentUpdateHandler);
						agentUpdateHandler = null;
						retryCount = 0;
						LOGGER.info("same or old agent version {} is sent for update. skip this.",
								message.getVersion());
						m_state = AgentControllerState.READY;
					} catch (Exception e) {
						retryCount = 0;
						IOUtils.closeQuietly(agentUpdateHandler);
						agentUpdateHandler = null;
						LOGGER.error("While updating agent, the exception occurred.", e);
						m_state = AgentControllerState.READY;
					}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				} else {
					// ConsoleListener.RESET or natural death.
					startMessage = null;
				}
			}

		} finally {
			m_connectionPort = 0;
			// Abnormal state.
			agent.shutdown();
			m_state = AgentControllerState.FINISHED;
			shutdownConsoleCommunication(consoleCommunication);
			m_timer.cancel();
		}
	}

	private void sendLog(ConsoleCommunication consoleCommunication, String testId) {
		File logFolder = new File(agentConfig.getHome().getLogDirectory(), testId);
		if (!logFolder.exists()) {
			return;
		}
		File[] logFiles = logFolder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return (name.endsWith(".log"));
			}
		});
<<<<<<< HEAD
		Arrays.sort(logFiles);
		if (ArrayUtils.isEmpty(logFiles)) {
			LOGGER.error("No log exists under {}", logFolder.getAbsolutePath());
			return;
		}

		// Take only one file... if agent.send.all.logs is not set.
		if (!agentConfig.getPropertyBoolean("agent.send.all.logs", false)) {
			logFiles = new File[] { logFiles[0] };
		}

		consoleCommunication.sendMessage(new LogReportGrinderMessage(testId, LogCompressUtil.compressFile(logFiles),
						new AgentAddress(m_agentIdentity)));
		// Delete logs to clean up
		FileUtils.deleteQuietly(logFolder);
=======

		if (logFiles == null || ArrayUtils.isEmpty(logFiles)) {
			LOGGER.error("No log exists under {}", logFolder.getAbsolutePath());
			return;
		}
		Arrays.sort(logFiles);
		// Take only one file... if agent.send.all.logs is not set.
		if (!agentConfig.getAgentProperties().getPropertyBoolean(PROP_AGENT_ALL_LOGS)) {
			logFiles = new File[]{logFiles[0]};
		}
		final byte[] compressedLog = LogCompressUtils.compress(logFiles,
				Charset.defaultCharset(), Charset.forName("UTF-8")
		);
		consoleCommunication.sendMessage(new LogReportGrinderMessage(testId, compressedLog, new AgentAddress(m_agentIdentity)));
		// Delete logs to clean up
		if (!agentConfig.getAgentProperties().getPropertyBoolean(PROP_AGENT_KEEP_LOGS)) {
			LOGGER.info("Clean up the perftest logs");
			FileUtils.deleteQuietly(logFolder);
		}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	private void shutdownConsoleCommunication(ConsoleCommunication consoleCommunication) {
		sendCurrentState(consoleCommunication);
		if (consoleCommunication != null) {
			consoleCommunication.shutdown();
<<<<<<< HEAD
=======
			//noinspection UnusedAssignment
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			consoleCommunication = null;
		}
		m_agentControllerServerListener.discardMessages(AgentControllerServerListener.ANY);
	}

	private void sendCurrentState(ConsoleCommunication consoleCommunication) {
		if (consoleCommunication != null) {
			try {
				consoleCommunication.sendCurrentState();
			} catch (CommunicationException e) {
				LOGGER.error("Error while sending current state : {}.", e.getMessage());
				LOGGER.debug("The error detail is ", e);
			}
		}
	}

	/**
	 * Clean up resources.
	 */
	public void shutdown() {
		if (m_timer != null) {
			m_timer.cancel();
		}
		if (m_fanOutStreamSender != null) {
			m_fanOutStreamSender.shutdown();
		}
		m_agentControllerServerListener.shutdown();
<<<<<<< HEAD
		LOGGER.info("finished");
=======
		LOGGER.info("Agent controller shuts down");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get current System performance.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link SystemDataModel} instance
	 */
	public SystemDataModel getSystemDataModel() {
		try {
			SystemInfo systemInfo = agentSystemDataCollector.execute();
			return new SystemDataModel(systemInfo, this.version);
		} catch (Exception e) {
<<<<<<< HEAD
			LOGGER.error("Error while getting system perf data model : {} ", e.getMessage());
=======
			LOGGER.error("Error while getting system data model : {} ", e.getMessage());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			LOGGER.debug("The error detail is ", e);
			return emptySystemDataModel;
		}
	}

	public AgentConfig getAgentConfig() {
		return agentConfig;
	}

<<<<<<< HEAD
	/**
	 * Set agent config.
	 * 
	 * @param agentConfig
	 *            agent config
	 */
	public void setAgentConfig(AgentConfig agentConfig) {
		this.agentConfig = agentConfig;
		this.version = agentConfig.getInternalProperty("ngrinder.version", "3.1.3");
		this.agentSystemDataCollector.setAgentHome(agentConfig.getHome().getDirectory());
	}

	private final class ConsoleCommunication {
=======

	public final class ConsoleCommunication {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		private final ClientSender m_sender;
		private final TimerTask m_reportRunningTask;
		private final MessagePump m_messagePump;

		public ConsoleCommunication(Connector connector) throws CommunicationException {
			final ClientReceiver receiver = ClientReceiver.connect(connector, new AgentAddress(m_agentIdentity));
			m_sender = ClientSender.connect(receiver);

			m_sender.send(new AgentControllerProcessReportMessage(AgentControllerState.STARTED, getSystemDataModel(),
<<<<<<< HEAD
							m_connectionPort));
=======
					m_connectionPort, version));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			final MessageDispatchSender messageDispatcher = new MessageDispatchSender();
			m_agentControllerServerListener.registerMessageHandlers(messageDispatcher);

			m_messagePump = new MessagePump(receiver, messageDispatcher, 1);

			m_reportRunningTask = new TimerTask() {
				public void run() {
					try {
						sendCurrentState();
					} catch (CommunicationException e) {
						cancel();
						LOGGER.error("Error while sending current state:" + e.getMessage());
						LOGGER.debug("The error detail is", e);
					}
				}
			};
		}

<<<<<<< HEAD
		public void sendMessage(AddressAwareMessage message) {
			try {
				m_sender.send(message);
			} catch (CommunicationException e) {
				LOGGER.error("{}. This error is ignorable if it doesn't occur much.", e.getMessage());
=======
		public void sendMessage(Message message) {
			try {
				m_sender.send(message);
			} catch (CommunicationException e) {
				LOGGER.error("{}. This error is not critical if it doesn't occur much.", e.getMessage());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			}
		}

		public void sendCurrentState() throws CommunicationException {
<<<<<<< HEAD
			sendMessage(new AgentControllerProcessReportMessage(m_state, getSystemDataModel(), m_connectionPort));
=======
			sendMessage(new AgentControllerProcessReportMessage(m_state, getSystemDataModel(), m_connectionPort, version));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}

		public void start() {
			m_messagePump.start();
			m_timer.schedule(m_reportRunningTask, 0, GrinderConstants.AGENT_CONTROLLER_HEARTBEAT_INTERVAL);
		}

		public void shutdown() {
			m_reportRunningTask.cancel();
			try {
<<<<<<< HEAD
				m_sender.send(new AgentControllerProcessReportMessage(AgentControllerState.FINISHED, null, 0));
=======
				m_sender.send(new AgentControllerProcessReportMessage(AgentControllerState.FINISHED, null, 0, version));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			} catch (CommunicationException e) {
				// Fall through
				// Ignore - peer has probably shut down.
				noOp();
			} finally {
				m_messagePump.shutdown();
			}
		}
<<<<<<< HEAD
=======

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}
}
