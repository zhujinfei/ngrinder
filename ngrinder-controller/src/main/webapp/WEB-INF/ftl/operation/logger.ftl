<!DOCTYPE html>
<html>
<head>
<#include "../common/common.ftl">
<#include "../common/datatables.ftl">
<<<<<<< HEAD
<title><@spring.message "log.view.title"/></title>
</head>

<body>
	<#include "../common/navigator.ftl">
	<div class="container">
		<div class="row">
			<div class="span12">
				<legend class="header">
					<@spring.message "navigator.dropdown.logMonitoring"/>
				</legend>
				<table id="log_container">
				</table>
			</div>
		</div>
		<#include "../common/copyright.ftl">
	</div>
	<script>
		// Wrap this function in a closure so we don't pollute the namespace
		(function pollingLogs() {
		    $.ajax({
			    url: '${req.getContextPath()}/operation/log/last', 
			    type: 'GET',
			    cache: false,
			    success: function(data) {
			    	var eachLog = $("tr#" +data.index + " td");
			    	if (eachLog.size() != 0) {
			    		if (eachLog.attr("id") != data.modification) {
			    			eachLog.html(data.log);
			    			eachLog.attr("id", data.modification);
			    		}
			    	} else {
			    		var logentries = $("#log_container tr");
			    		if (logentries.size() > 5) {
			    			logentries.first().remove();
			    		}
			    		$("#log_container").append($("<tr id='" + data.index + "'><td id='" + data.modification + "'>" + data.log + "</td></tr>"));
			    	}
			    	
			    	setTimeout(pollingLogs, 5000);
			    }
		    });
	  })();
=======
<title><@spring.message "operation.log.title"/></title>
<style>
	div.row {
		margin-bottom: 50px;
	}
</style>
</head>

<body>
<div id="wrap">
	<#include "../common/navigator.ftl">
	<div class="container">
		<legend class="header">
			<@spring.message "navigator.dropDown.logMonitoring"/>
		</legend>
		<table id="log_container">
		</table>
	</div>
</div>
<#include "../common/copyright.ftl">
	<script>
		// Wrap this function in a closure so we don't pollute the namespace
		(function pollingLogs() {
			var ajaxObj = new AjaxObj("/operation/log/last");
			ajaxObj.success = function(data) {
				var eachLog = $("tr#" +data.index + " td");
				if (eachLog.size() != 0) {
					//noinspection JSUnresolvedVariable
					if (eachLog.attr("id") != data.modification) {
						eachLog.html(data.log);
						//noinspection JSUnresolvedVariable
						eachLog.attr("id", data.modification);
					}
				} else {
					var $logContainer = $("#log_container");
					var logEntries = $logContainer.find("tr");
					if (logEntries.size() > 5) {
						logEntries.first().remove();
					}
					$logContainer.append($("<tr id='" + data.index + "'><td id='" + data.modification + "'>" + data.log + "</td></tr>"));
				}
				setTimeout(pollingLogs, 5000);
			};
            ajaxObj.call();
		})();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	</script>
</body>
</html>