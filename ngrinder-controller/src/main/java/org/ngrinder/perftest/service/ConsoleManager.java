/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.perftest.service;

<<<<<<< HEAD
import static org.ngrinder.common.constant.NGrinderConstants.NGRINDER_PROP_CONSOLE_MAX_WAITING_MILLISECONDS;
import static org.ngrinder.common.util.ExceptionUtils.processException;
import static org.ngrinder.common.util.NoOp.noOp;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import net.grinder.SingleConsole;
import net.grinder.console.model.ConsoleProperties;

import org.ngrinder.common.constant.NGrinderConstants;
import org.ngrinder.common.exception.NGrinderRuntimeException;
=======
import net.grinder.SingleConsole;
import net.grinder.console.model.ConsoleProperties;
import org.h2.util.StringUtils;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.infra.config.Config;
import org.ngrinder.perftest.model.NullSingleConsole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
<<<<<<< HEAD
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Console manager is responsible for console instance management.<br/>
=======
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import static net.grinder.util.NetworkUtils.getAvailablePorts;
import static org.ngrinder.common.constant.ControllerConstants.*;
import static org.ngrinder.common.util.ExceptionUtils.processException;
import static org.ngrinder.common.util.NoOp.noOp;

/**
 * Console manager is responsible for console instance management.
 * <p/>
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * A number of consoles(specified in ngrinder.maxConcurrentTest in system.conf) are pooled. Actually console itself is
 * not pooled but the {@link ConsoleEntry} which contains console information are pooled internally. Whenever a user
 * requires a new console, it gets the one {@link ConsoleEntry} from the pool and creates new console with the
 * {@link ConsoleEntry}. Currently using consoles are kept in {@link #consoleInUse} member variable.
<<<<<<< HEAD
 * 
=======
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author JunHo Yoon
 * @since 3.0
 */
@Component
public class ConsoleManager {
	private static final int MAX_PORT_NUMBER = 65000;
	private static final Logger LOG = LoggerFactory.getLogger(ConsoleManager.class);
	private volatile ArrayBlockingQueue<ConsoleEntry> consoleQueue;
	private volatile List<SingleConsole> consoleInUse = Collections.synchronizedList(new ArrayList<SingleConsole>());

	@Autowired
	private Config config;

	@Autowired
	private AgentManager agentManager;

<<<<<<< HEAD
=======

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	/**
	 * Prepare console queue.
	 */
	@PostConstruct
	public void init() {
		int consoleSize = getConsoleSize();
		consoleQueue = new ArrayBlockingQueue<ConsoleEntry>(consoleSize);
<<<<<<< HEAD
		for (int each : getAvailablePorts(consoleSize, getConsolePortBase())) {
			consoleQueue.add(new ConsoleEntry(each));
=======
		final String currentIP = config.getCurrentIP();
		for (int each : getAvailablePorts(currentIP, consoleSize, getConsolePortBase(), MAX_PORT_NUMBER)) {
			final ConsoleEntry e = new ConsoleEntry(config.getCurrentIP(), each);
			try {
				e.occupySocket();
				consoleQueue.add(e);
			} catch (Exception ex) {
				LOG.error("socket binding to {}:{} is failed", config.getCurrentIP(), each);
			}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}

	/**
<<<<<<< HEAD
	 * Get the base port number of console.<br/>
	 * It can be specified at ngrinder.consolePortBase in system.conf. Each console will be created from that port.
	 * 
	 * @return base port number
	 */
	protected int getConsolePortBase() {
		return config.getSystemProperties().getPropertyInt(NGrinderConstants.NGRINDER_PROP_CONSOLE_PORT_BASE,
				NGrinderConstants.NGRINDER_PROP_CONSOLE_PORT_BASE_VALUE);
=======
	 * Get the base port number of console.
	 * <p/>
	 * It can be specified at ngrinder.consolePortBase in system.conf. Each console will be created from that port.
	 *
	 * @return base port number
	 */
	protected int getConsolePortBase() {
		return config.getControllerProperties().getPropertyInt(PROP_CONTROLLER_CONSOLE_PORT_BASE);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the console pool size. It can be specified at ngrinder.maxConcurrentTest in system.conf.
<<<<<<< HEAD
	 * 
	 * @return console size.
	 */
	protected int getConsoleSize() {
		return config.getSystemProperties().getPropertyInt(NGrinderConstants.NGRINDER_PROP_MAX_CONCURRENT_TEST,
				NGrinderConstants.NGRINDER_PROP_MAX_CONCURRENT_TEST_VALUE);
=======
	 *
	 * @return console size.
	 */
	protected int getConsoleSize() {
		return config.getControllerProperties().getPropertyInt(PROP_CONTROLLER_MAX_CONCURRENT_TEST);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get Timeout (in second).
<<<<<<< HEAD
	 * 
	 * @return 5000 second
	 */
	protected long getMaxWaitingMiliSecond() {
		return config.getSystemProperties().getPropertyInt(NGRINDER_PROP_CONSOLE_MAX_WAITING_MILLISECONDS,
				NGrinderConstants.NGRINDER_PROP_CONSOLE_MAX_WAITING_MILLISECONDS_VALUE);
	}

	/**
	 * Get the available ports.
	 * 
	 * @param size
	 *            port size
	 * @param from
	 *            port number starting from
	 * @return port list
	 */
	List<Integer> getAvailablePorts(int size, int from) {
		List<Integer> ports = new ArrayList<Integer>();
		int freeSocket;
		for (int i = 0; i < size; i++) {
			freeSocket = checkPortAvailability(from);
			ports.add(freeSocket);
			from = freeSocket + 1;
		}
		return ports;
	}

	/**
	 * Get a available port greater than the given port.
	 * 
	 * @param scanStartPort
	 *            port scan from
	 * @return min port available from scanStartPort
	 */
	private int checkPortAvailability(int scanStartPort) {
		while (true) {
			if (checkExactPortAvailability(scanStartPort)) {
				return scanStartPort;
			}
			if (scanStartPort++ > MAX_PORT_NUMBER) {
				throw processException("no port for console is available");
			}
		}
	}

	/**
	 * Check if the given port is available.
	 * 
	 * @param port
	 *            port to be checked
	 * @return true if available
	 */
	private boolean checkExactPortAvailability(int port) {
		ServerSocket socket = null;
		try {
			socket = new ServerSocket(port);
			return true;
		} catch (IOException e) {
			return false;
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					// FALL THROUGH
					noOp();
				}
			}
		}
	}

	/**
	 * Get a available console.<br/>
	 * 
	 * If there is no available console, it waits until available console is returned back. If the specific time is
	 * elapsed, the timeout error occurs and throws {@link NGrinderRuntimeException} . The timeout can be adjusted by
	 * overriding {@link #getMaxWaitingMiliSecond()}.
	 * 
	 * @param testIdentifier
	 *            test identifier
	 * @param baseConsoleProperties
	 *            base {@link ConsoleProperties}
	 * @return console
	 */
	public SingleConsole getAvailableConsole(String testIdentifier, ConsoleProperties baseConsoleProperties) {
		ConsoleEntry consoleEntry = null;
		SingleConsole singleConsole = null;
		try {
			consoleEntry = consoleQueue.poll(getMaxWaitingMiliSecond(), TimeUnit.MILLISECONDS);
=======
	 *
	 * @return 5000 second
	 */
	protected long getMaxWaitingMilliSecond() {
		return config.getControllerProperties().getPropertyInt(PROP_CONTROLLER_MAX_CONNECTION_WAITING_MILLISECOND);
	}


	/**
	 * Get an available console.
	 * <p/>
	 * If there is no available console, it waits until available console is returned back. If the specific time is
	 * elapsed, the timeout error occurs and throws {@link org.ngrinder.common.exception.NGrinderRuntimeException} . The
	 * timeout can be adjusted by overriding {@link #getMaxWaitingMilliSecond()}.
	 *
	 * @param baseConsoleProperties base {@link net.grinder.console.model.ConsoleProperties}
	 * @return console
	 */
	public SingleConsole getAvailableConsole(ConsoleProperties baseConsoleProperties) {
		ConsoleEntry consoleEntry = null;
		try {
			consoleEntry = consoleQueue.poll(getMaxWaitingMilliSecond(), TimeUnit.MILLISECONDS);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			if (consoleEntry == null) {
				throw processException("no console entry available");
			}
			synchronized (this) {
<<<<<<< HEAD
				// FIXME : It might fail here
				singleConsole = new SingleConsole(config.getCurrentIP(), consoleEntry.getPort(), baseConsoleProperties);
=======
				consoleEntry.releaseSocket();
				// FIXME : It might fail here
				SingleConsole singleConsole = new SingleConsole(config.getCurrentIP(), consoleEntry.getPort(),
						baseConsoleProperties);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				getConsoleInUse().add(singleConsole);
				return singleConsole;
			}
		} catch (Exception e) {
			if (consoleEntry != null) {
				consoleQueue.add(consoleEntry);
			}
			throw processException("no console entry available");
		}
	}

	/**
<<<<<<< HEAD
	 * Return back the given console.<br/>
	 * 
	 * Duplicated returns is allowed.
	 * 
	 * @param testIdentifier
	 *            test identifier
	 * @param console
	 *            console which will be returned back.
	 * 
	 */
	@Async
	public void returnBackConsole(String testIdentifier, SingleConsole console) {
		if (console == null) {
			LOG.error("Attemp to return back null console for {}.", testIdentifier);
=======
	 * Return back the given console.
	 * <p/>
	 * Duplicated returns is allowed.
	 *
	 * @param testIdentifier test identifier
	 * @param console        console which will be returned back.
	 */
	public void returnBackConsole(String testIdentifier, SingleConsole console) {
		if (console == null || console instanceof NullSingleConsole) {
			LOG.error("Attempt to return back null console for {}.", testIdentifier);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			return;
		}
		try {
			console.sendStopMessageToAgents();
		} catch (Exception e) {
<<<<<<< HEAD
			LOG.error("Exception is occurred while shuttdowning console in returnback process for test {}.",
=======
			LOG.error("Exception occurred during console return back for test {}.",
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					testIdentifier, e);
			// But the port is getting back.
		} finally {
			// This is very careful implementation..
			try {
				// Wait console is completely shutdown...
				console.waitUntilAllAgentDisconnected();
			} catch (Exception e) {
<<<<<<< HEAD
				LOG.error("Exception occurs while shuttdowning console in returnback process for test {}.",
						testIdentifier, e);
				// If it's not disconnected still, stop them forcely.
=======
				LOG.error("Exception occurred during console return back for test {}.",
						testIdentifier, e);
				// If it's not disconnected still, stop them by force.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				agentManager.stopAgent(console.getConsolePort());
			}
			try {
				console.shutdown();
			} catch (Exception e) {
<<<<<<< HEAD
				LOG.error("Exception occurs while shuttdowning console in returnback process for test {}.",
						testIdentifier, e);
			}
			int consolePort = console.getConsolePort();
			if (consolePort == 1) {
				return;
			}
			ConsoleEntry consoleEntry = new ConsoleEntry(consolePort);
			synchronized (this) {
				if (!consoleQueue.contains(consoleEntry)) {
					consoleQueue.add(consoleEntry);
					if (!getConsoleInUse().contains(console)) {
						LOG.error("Try to return back the not used console on {} port", consolePort);
					}
					getConsoleInUse().remove(console);
				}
=======
				LOG.error("Exception occurred during console return back for test {}.",
						testIdentifier, e);
			}
			int consolePort;
			String consoleIP;
			try {
				consolePort = console.getConsolePort();
				consoleIP = console.getConsoleIP();
				ConsoleEntry consoleEntry = new ConsoleEntry(consoleIP, consolePort);
				synchronized (this) {
					if (!consoleQueue.contains(consoleEntry)) {
						consoleEntry.occupySocket();
						consoleQueue.add(consoleEntry);
						if (!getConsoleInUse().contains(console)) {
							LOG.error("Try to return back the not used console on {} port", consolePort);
						}
						getConsoleInUse().remove(console);
					}
				}
			} catch (Exception e) {
				noOp();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			}
		}
	}

	/**
	 * Get the list of {@link SingleConsole} which are used.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link SingleConsole} list in use
	 */
	public List<SingleConsole> getConsoleInUse() {
		return consoleInUse;
	}

	/**
	 * Get the size of currently available consoles.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return size of available consoles.
	 */
	public Integer getAvailableConsoleSize() {
		return consoleQueue.size();
	}

	/**
	 * Get the {@link SingleConsole} instance which is using the given port.
<<<<<<< HEAD
	 * 
	 * @param port
	 *            port which the console is using
	 * @return {@link SingleConsole} instance if found. Otherwise, {@link NullSingleConsole} instance.
	 */
	public SingleConsole getConsoleUsingPort(Integer port) {
		for (SingleConsole each : consoleInUse) {
			if (each.getConsolePort() == port) {
=======
	 *
	 * @param port port which the console is using
	 * @return {@link SingleConsole} instance if found. Otherwise, {@link NullSingleConsole} instance.
	 */
	public SingleConsole getConsoleUsingPort(Integer port) {
		String currentIP = config.getCurrentIP();
		for (SingleConsole each : consoleInUse) {
			// Avoid to Klocwork error.
			if (each instanceof NullSingleConsole) {
				continue;
			}
			if (StringUtils.equals(each.getConsoleIP(), currentIP) && each.getConsolePort() == port) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				return each;
			}
		}
		return new NullSingleConsole();
	}

}
