/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.infra.servlet;

<<<<<<< HEAD
=======
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Set;

<<<<<<< HEAD
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * {@link ServletContext} delegate handler which forwards a request to the passed file path.
 * 
=======
/**
 * {@link ServletContext} delegate handler which forwards a request to the passed file path.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author JunHo Yoon
 * @since 3.2
 */
@SuppressWarnings("deprecation")
public class ServletContextDelegate implements ServletContext {
	private final ServletContext servletContext;
	private final File base;

	/**
	 * Constructor.
<<<<<<< HEAD
	 * 
	 * @param servletContext
	 *            servlet context
	 * @param base
	 *            the base directory to which the request is forwarded.
=======
	 *
	 * @param servletContext servlet context
	 * @param base           the base directory to which the request is forwarded.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public ServletContextDelegate(ServletContext servletContext, File base) {
		this.servletContext = servletContext;
		this.base = base;
	}

	@Override
<<<<<<< HEAD
	public ServletContext getContext(String uripath) {
		return servletContext.getContext(uripath);
=======
	public String getContextPath() {
		return "/";  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public ServletContext getContext(String uriPath) {
		return servletContext.getContext(uriPath);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	@Override
	public int getMajorVersion() {
		return servletContext.getMajorVersion();
	}

	@Override
	public int getMinorVersion() {
		return servletContext.getMinorVersion();
	}

	@Override
	public String getMimeType(String file) {
		return servletContext.getMimeType(file);
	}

	@Override
	public URL getResource(String path) throws MalformedURLException {
		return new File(base, hackXBean(path)).toURI().toURL();
	}

	private String hackXBean(String path) {
		if (path.contains("xbean.jar")) {
			path = path.replace("xbean.jar", "xbean-1.0.jar");
		}
		return path;
	}

	@Override
	public InputStream getResourceAsStream(String path) {
		try {
			return new FileInputStream(new File(base, hackXBean(path)));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public RequestDispatcher getRequestDispatcher(String path) {
		return servletContext.getRequestDispatcher(path);
	}

	@Override
	public RequestDispatcher getNamedDispatcher(String name) {
		return servletContext.getNamedDispatcher(name);
	}

	@Override
	public String getRealPath(String path) {
		return new File(base, hackXBean(path)).getAbsolutePath();
	}

	@Override
	public String getInitParameter(String name) {
		return servletContext.getInitParameter(name);
	}

<<<<<<< HEAD
	@Override
	public Enumeration<?> getInitParameterNames() {
=======
	@SuppressWarnings("unchecked")
	@Override
	public Enumeration<String> getInitParameterNames() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return servletContext.getInitParameterNames();
	}

	@Override
	public Object getAttribute(String name) {
		return servletContext.getAttribute(name);
	}

<<<<<<< HEAD
	@Override
	public Enumeration<?> getAttributeNames() {
		return servletContext.getAttributeNames();
	}

	@Override
	public Set<?> getResourcePaths(String arg0) {
=======
	@SuppressWarnings("unchecked")
	@Override
	public Enumeration<String> getAttributeNames() {
		return servletContext.getAttributeNames();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<String> getResourcePaths(String arg0) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return servletContext.getResourcePaths(arg0);
	}

	@Override
	public Servlet getServlet(String name) throws ServletException {
		return servletContext.getServlet(name);
	}

<<<<<<< HEAD
	@Override
	public Enumeration<?> getServlets() {
=======
	@SuppressWarnings("unchecked")
	@Override
	public Enumeration<String> getServlets() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return servletContext.getServlets();
	}

	@Override
	public void log(String msg) {
		servletContext.log(msg);
	}

	@Override
	public void log(Exception exception, String msg) {
		servletContext.log(exception, msg);
	}

	@Override
	public void log(String message, Throwable throwable) {
		servletContext.log(message, throwable);
	}

	@Override
	public String getServerInfo() {
		return servletContext.getServerInfo();
	}

	@Override
	public String getServletContextName() {
		return servletContext.getServletContextName();
	}

<<<<<<< HEAD
	@Override
	public Enumeration<?> getServletNames() {
=======
	@SuppressWarnings("unchecked")
	@Override
	public Enumeration<String> getServletNames() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return servletContext.getServletNames();
	}

	@Override
	public void setAttribute(String name, Object object) {
		servletContext.setAttribute(name, object);
	}

	@Override
	public void removeAttribute(String name) {
		servletContext.removeAttribute(name);
	}
}
